# Java chess

This is a chess engine written from scratch in Java for learning purposes.

## Command line
To play over command line, run the CommandLine file. Both single player and multiplayer modes are available. Moves should be formatted like this: <starting cell> <target cell>. E.g. e2 e4.

## Play by SMS
To play by SMS you need to have a Twilio account. But first you need to host the app to make it publicly available over internet. Edit the SmsApp file and add the phone numbers you want to use for playing. One option for hosting is [Heroku](https://devcenter.heroku.com/articles/getting-started-with-java?singlepage=true).

Once the app is up and running. Go to your [Twilio console numbers section](https://www.twilio.com/console/phone-numbers/incoming) and add a messaging POST webhook pointing to the app you deployed in the previous step. The webhook should look something like this: \<your app base URL>/play

After that you can send messages to your Twilio number. The move will be processed and current board state will be sent to the second player, who has to make a move next.
To start a new game send a message with text `new game`. Moves should be formatted like this: \<starting cell> \<target cell>. E.g. `e2 e4`.