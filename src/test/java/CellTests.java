import chessEngine.Board;
import chessEngine.Cell;
import chessEngine.Color;
import chessEngine.Knight;
import org.junit.Test;

public class CellTests {

    @Test
    public void cellToStringTest() {
        Cell cell = new Cell(0, 1, new Board());
        assert cell.toString().equals("B1");
    }

    @Test
    public void cellWithinLimitTest() {
        Cell impossibleCell = new Cell(-1, -1, new Board());
        Cell impossibleCell2 = new Cell(8, 7, new Board());
        Cell normalCell = new Cell(1, 1, new Board());
        assert !impossibleCell.isWithinLimits();
        assert !impossibleCell2.isWithinLimits();
        assert normalCell.isWithinLimits();
    }

    @Test
    public void putPieceTest() {
        Cell cell = new Cell(1, 1, new Board());
        Knight knight = new Knight(Color.WHITE);
        cell.setPiece(knight);
        assert cell.getPiece().equals(knight);
    }

    @Test
    public void clearCellTest() {
        Cell cell = new Cell(1, 1, new Board());
        Knight knight = new Knight(Color.WHITE);
        cell.setPiece(knight);
        cell.clear();
        assert cell.getPiece() == null;
    }

}
