import chessEngine.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashSet;
import java.util.Set;

public class PawnTests {
    @Test
    public void getWhiteRankTest() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, board.getCellByCoordinates(1, 3));
        assert pawn.getRank() == 2;
    }

    @Test
    public void getBlackRankTest() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.BLACK);
        board.putPiece(pawn, board.getCellByCoordinates(6, 3));
        assert pawn.getRank() == 2;
    }

    @Test
    public void getWhiteMovesWithDoubleMove() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, board.getCellByCoordinates(1, 4));
        Set<Cell> moves = new HashSet<>();
        Cell move1 = board.getCellByCoordinates(2, 4);
        Cell move2 = board.getCellByCoordinates(3, 4);
        moves.add(move1);
        moves.add(move2);
        assert pawn.getMoves().equals(moves);
    }

    @Test
    public void getWhiteMovesWithoutDoubleMove() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, board.getCellByCoordinates(2, 4));
        Set<Cell> moves = new HashSet<>();
        Cell move1 = board.getCellByCoordinates(3, 4);
        moves.add(move1);
        assert pawn.getMoves().equals(moves);
    }

    @Test
    public void getBlackovesWithDoubleMove() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.BLACK);
        board.putPiece(pawn, board.getCellByCoordinates(6, 4));
        Set<Cell> moves = new HashSet<>();
        Cell move1 = board.getCellByCoordinates(5, 4);
        Cell move2 = board.getCellByCoordinates(4, 4);
        moves.add(move1);
        moves.add(move2);
        assert pawn.getMoves().equals(moves);
    }

    @Test
    public void getBlackMovesWithoutDoubleMove() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.BLACK);
        board.putPiece(pawn, board.getCellByCoordinates(5, 4));
        Set<Cell> moves = new HashSet<>();
        Cell move1 = board.getCellByCoordinates(4, 4);
        moves.add(move1);
        assert pawn.getMoves().equals(moves);
    }

    @Test
    public void pawnMoveSingleTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(1, 1);
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCellByCoordinates(2, 1);
        pawn.move(targetCell);
        System.out.println(board);
        assert initialCell.isEmpty();
        assert targetCell.getPiece().equals(pawn);
        assert board.getPieces().contains(pawn);
    }

    @Test
    public void pawnMoveDoubleTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(1, 1);
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCellByCoordinates(3, 1);
        System.out.println(pawn.getMovedDoubleTimer());
        pawn.move(targetCell);
        System.out.println(board);
        System.out.println(pawn.getMovedDoubleTimer());
        assert initialCell.isEmpty();
        assert targetCell.getPiece().equals(pawn);
        assert board.getPieces().contains(pawn);
    }

    @Test
    public void getWhitePawnTargets() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, board.getCellByCoordinates(1, 4));
        Knight knight = new Knight(Color.BLACK);
        board.putPiece(knight, board.getCellByCoordinates(2, 5));
        Bishop bishop = new Bishop(Color.BLACK);
        board.putPiece(bishop, board.getCellByCoordinates(2, 4));
        Queen queen = new Queen(Color.BLACK);
        board.putPiece(queen,  board.getCellByCoordinates(2, 3));
        Rook rook = new Rook((Color.BLACK));
        board.putPiece(rook, board.getCellByCoordinates(0, 3));
        Set<Cell> targets = new HashSet<>();
        Cell target1 = board.getCellByCoordinates(2, 5);
        Cell target2 = board.getCellByCoordinates(2, 3);
        targets.add(target1);
        targets.add(target2);
        assert pawn.getTargets().equals(targets);
    }

    @Test
    public void getBlackPawnTargets() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.BLACK);
        board.putPiece(pawn, board.getCellByCoordinates(1, 4));
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, board.getCellByCoordinates(2, 5));
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, board.getCellByCoordinates(2, 4));
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen,  board.getCellByCoordinates(2, 3));
        Rook rook = new Rook((Color.WHITE));
        board.putPiece(rook, board.getCellByCoordinates(0, 3));
        Set<Cell> targets = new HashSet<>();
        Cell target1 = board.getCellByCoordinates(0, 3);
        targets.add(target1);
        assert pawn.getTargets().equals(targets);
    }

    @Test
    public void getPawnTargetsOnSide() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.BLACK);
        board.putPiece(pawn, board.getCellByCoordinates(1, 0));
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, board.getCellByCoordinates(2, 5));
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, board.getCellByCoordinates(2, 4));
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen,  board.getCellByCoordinates(2, 3));
        Rook rook = new Rook((Color.WHITE));
        board.putPiece(rook, board.getCellByCoordinates(0, 1));
        Set<Cell> targets = new HashSet<>();
        Cell target1 = board.getCellByCoordinates(0, 1);
        targets.add(target1);
        assert pawn.getTargets().equals(targets);
    }

    @Test
    public void pawnCaptureTest() {
        Board board = new Board();
        Cell pawnCell = board.getCellByCoordinates(1, 2);
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, pawnCell);
        Cell blackKnightCell = board.getCellByCoordinates(2, 3);
        Knight blackKnight = new Knight(Color.BLACK);
        board.putPiece(blackKnight, blackKnightCell);
        System.out.println(board);
        Cell targetCell = blackKnightCell;
        assert targetCell.getPiece() == blackKnight;
        assert board.getPieces().contains(blackKnight);
        pawn.capture(targetCell);
        System.out.println(board);
        assert targetCell.getPiece() == pawn;
        assert !board.getPieces().contains(blackKnight);
    }

    @Test
    public void getWhitePawnEnPassantTargets() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, board.getCellByCoordinates(4, 4));
        Pawn pawn2 = new Pawn(Color.BLACK);
        board.putPiece(pawn2, board.getCellByCoordinates(6, 5));
        Pawn pawn3 = new Pawn(Color.BLACK);
        board.putPiece(pawn3, board.getCellByCoordinates(4, 3));
        pawn2.move(board.getCellByCoordinates(4, 5));
        Set<Cell> targets = new HashSet<>();
        Cell target1 = board.getCellByCoordinates(5, 5);
        targets.add(target1);
        assert pawn.getEnPassantTargets().equals(targets);
    }

    @Test
    public void getBlackPawnEnPassantTargets() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.BLACK);
        board.putPiece(pawn, board.getCellByCoordinates(3, 4));
        Pawn pawn2 = new Pawn(Color.WHITE);
        board.putPiece(pawn2, board.getCellByCoordinates(1, 5));
        Pawn pawn3 = new Pawn(Color.WHITE);
        board.putPiece(pawn3, board.getCellByCoordinates(3, 3));
        pawn2.move(board.getCellByCoordinates(3, 5));
        Set<Cell> targets = new HashSet<>();
        Cell target1 = board.getCellByCoordinates(2, 5);
        targets.add(target1);
        assert pawn.getEnPassantTargets().equals(targets);
    }

    @Test
    public void whitePawnEnPassantCaptureTest() {
        Board board = new Board();
        Pawn pawn = new Pawn(Color.WHITE);
        board.putPiece(pawn, board.getCellByCoordinates(4, 4));
        Cell pawn2StartingCell = board.getCellByCoordinates(6, 5);
        Pawn pawn2 = new Pawn(Color.BLACK);
        board.putPiece(pawn2, pawn2StartingCell);
        Cell pawn2DestinationCell = board.getCellByCoordinates(4, 5);
        pawn2.move(pawn2DestinationCell);
        Cell target1 = board.getCellByCoordinates(5, 5);
        System.out.println(board);
        assert pawn2DestinationCell.getPiece() == pawn2;
        assert board.getPieces().contains(pawn2);
        pawn.capture(target1);
        System.out.println(board);
        assert target1.getPiece() == pawn;
        assert pawn2DestinationCell.isEmpty();
        assert !board.getPieces().contains(pawn2);
    }

    @Test
    public void enPassantCaptureTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("e4 e5");
        game.playTurn("a7 a6");
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            game.playTurn("e5 d6");
        });
        game.playTurn("a2 a3");
        game.playTurn("f7 f5");
        System.out.println(board);
        Pawn pawn = (Pawn) board.getPieceByCell(board.getCellByCoordinates(4, 5));
        System.out.println(pawn.getMovedDoubleTimer());
        game.playTurn("e5 f6");
        System.out.println(board);
    }
}
