import chessEngine.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashSet;
import java.util.Set;

public class KingTests {
    @Test
    public void kingGetMovesTest() {
        Board board = new Board();
        Cell targetCell = board.getCellByCoordinates(0, 0);
        King king = new King(Color.WHITE);
        board.putPiece(king, targetCell);
        System.out.println(board);
        Set<Cell> kingMoves = king.getMoves();
        Set<Cell> moves = new HashSet<>();
        Cell move1 = board.getCellByCoordinates(0, 1);
        Cell move2 = board.getCellByCoordinates(1, 0);
        Cell move3 = board.getCellByCoordinates(1, 1);
        moves.add(move1);
        moves.add(move2);
        moves.add(move3);
        assert kingMoves.equals(moves);
    }

    @Test
    public void kingMoveTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(0, 3);
        King king = new King(Color.WHITE);
        board.putPiece(king, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCellByCoordinates(1, 4);
        king.move(targetCell);
        System.out.println(board);
        assert initialCell.isEmpty();
        assert targetCell.getPiece().equals(king);
        assert board.getPieces().contains(king);
    }

    @Test
    public void kingCaptureTest() {
        Board board = new Board();
        Cell whiteKingCell = board.getCellByCoordinates(0, 1);
        King whiteKing = new King(Color.WHITE);
        board.putPiece(whiteKing, whiteKingCell);
        Cell blackKnightCell = board.getCellByCoordinates(0, 2);
        Knight blackKnight = new Knight(Color.BLACK);
        board.putPiece(blackKnight, blackKnightCell);
        System.out.println(board);
        Cell targetCell = blackKnightCell;
        assert targetCell.getPiece() == blackKnight;
        assert board.getPieces().contains(blackKnight);
        whiteKing.capture(targetCell);
        System.out.println(board);
        assert targetCell.getPiece() == whiteKing;
        assert !board.getPieces().contains(blackKnight);
    }

    @Test
    public void kingIsCastlingPathFreeTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(0, 4);
        King king = new King(Color.WHITE);
        board.putPiece(king, initialCell);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, board.getCellByCoordinates(0, 7));
        Rook rook2 = new Rook(Color.WHITE);
        board.putPiece(rook2, board.getCellByCoordinates(0, 0));
        System.out.println(board);
        assert king.isCastlingPathFree(rook);
        assert king.isCastlingPathFree(rook2);
    }

    @Test
    public void kingIsCastlingPathBlockedTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(0, 4);
        King king = new King(Color.WHITE);
        board.putPiece(king, initialCell);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, board.getCellByCoordinates(0, 7));
        Rook rook2 = new Rook(Color.WHITE);
        board.putPiece(rook2, board.getCellByCoordinates(0, 0));
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, board.getCellByCoordinates(0, 1));
        Knight knight2 = new Knight(Color.WHITE);
        board.putPiece(knight2, board.getCellByCoordinates(0, 6));
        System.out.println(board);
        assert !king.isCastlingPathFree(rook);
        assert !king.isCastlingPathFree(rook2);
    }

    @Test
    public void kingCastleKingsideTest() {
        Board board = new Board();
        Cell kingStartingCell = board.getCellByCoordinates(0, 4);
        King king = new King(Color.WHITE);
        board.putPiece(king, kingStartingCell);
        Cell rookStartingCell = board.getCellByCoordinates(0, 7);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, rookStartingCell);
        System.out.println(board);
        king.castle(rook);
        System.out.println(board);
        Cell kingTargetCell = board.getCellByCoordinates(0, 6);
        Cell rookTargetCell = board.getCellByCoordinates(0, 5);
        assert kingStartingCell.isEmpty();
        assert rookStartingCell.isEmpty();
        assert kingTargetCell.getPiece().equals(king);
        assert rookTargetCell.getPiece().equals(rook);
        assert board.getPieces().contains(king);
        assert board.getPieces().contains(rook);
    }

    @Test
    public void kingCastleQueensideTest() {
        Board board = new Board();
        Cell kingStartingCell = board.getCellByCoordinates(7, 4);
        King king = new King(Color.BLACK);
        board.putPiece(king, kingStartingCell);
        Cell rookStartingCell = board.getCellByCoordinates(7, 0);
        Rook rook = new Rook(Color.BLACK);
        board.putPiece(rook, rookStartingCell);
        System.out.println(board);
        king.castle(rook);
        System.out.println(board);
        Cell kingTargetCell = board.getCellByCoordinates(7, 2);
        Cell rookTargetCell = board.getCellByCoordinates(7, 3);
        assert kingStartingCell.isEmpty();
        assert rookStartingCell.isEmpty();
        assert kingTargetCell.getPiece().equals(king);
        assert rookTargetCell.getPiece().equals(rook);
        assert board.getPieces().contains(king);
        assert board.getPieces().contains(rook);
    }

    @Test
    public void castleQueensideFailsWhenCastlingThroughCheckTest() {
        Board board = new Board();
        Cell kingStartingCell = board.getCellByCoordinates(7, 4);
        King king = new King(Color.BLACK);
        board.putPiece(king, kingStartingCell);
        Cell rookStartingCell = board.getCellByCoordinates(7, 0);
        Rook rook = new Rook(Color.BLACK);
        board.putPiece(rook, rookStartingCell);
        Bishop enemyBishop = new Bishop((Color.WHITE));
        board.putPiece(enemyBishop, board.getCellByCoordinates(5, 5));
        System.out.println(board);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            king.castle(rook);
        });
    }

    @Test
    public void castleQueensideWorksWhenOnlyRookMovesThroughCheckTest() {
        Board board = new Board();
        Cell kingStartingCell = board.getCellByCoordinates(7, 4);
        King king = new King(Color.BLACK);
        board.putPiece(king, kingStartingCell);
        Cell rookStartingCell = board.getCellByCoordinates(7, 0);
        Rook rook = new Rook(Color.BLACK);
        board.putPiece(rook, rookStartingCell);
        Bishop enemyBishop = new Bishop((Color.WHITE));
        board.putPiece(enemyBishop, board.getCellByCoordinates(4, 4));
        System.out.println(board);
        Cell kingTargetCell = board.getCellByCoordinates(7, 2);
        Cell rookTargetCell = board.getCellByCoordinates(7, 3);
        king.castle(rook);
        System.out.println(board);
        assert kingStartingCell.isEmpty();
        assert rookStartingCell.isEmpty();
        assert kingTargetCell.getPiece().equals(king);
        assert rookTargetCell.getPiece().equals(rook);
        assert board.getPieces().contains(king);
        assert board.getPieces().contains(rook);
    }
}
