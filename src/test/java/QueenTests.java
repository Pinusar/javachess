import chessEngine.*;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class QueenTests {
    @Test
    public void queenGetMovesTest() {
        Board board = new Board();
        Cell targetCell = new Cell(0, 0, board);
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen, targetCell);

        Cell bishopCell = new Cell(0, 2);
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, bishopCell);

        Cell knightCell = new Cell(3, 0);
        Knight knight = new Knight(Color.BLACK);
        board.putPiece(knight, knightCell);

        Cell rookCell = board.getCellByCoordinates(3, 3);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, rookCell);

        Set<Cell> queenMoves = queen.getMoves();
        System.out.println(queenMoves);
        System.out.println(board);
        Set<Cell> moves = new HashSet<Cell>();
        Cell move1 = new Cell(0, 1, board);
        Cell move2 = new Cell(1, 0, board);
        Cell move3 = new Cell(2, 0, board);
        Cell move4 = new Cell(1, 1, board);
        Cell move5 = new Cell(2, 2, board);
        moves.add(move1);
        moves.add(move2);
        moves.add(move3);
        moves.add(move4);
        moves.add(move5);
        assert queenMoves.equals(moves);
    }

    @Test
    public void queenMoveTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(0, 0);
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCellByCoordinates(0,7);
        queen.move(targetCell);
        Cell targetCell2 = board.getCellByCoordinates(7,0);
        queen.move(targetCell2);
        System.out.println(board);
        assert board.getPieces().contains(queen);
        assert board.getCellByCoordinates(0, 0).isEmpty();
        assert board.getCellByCoordinates(0, 7).isEmpty();
        assert board.getCellByCoordinates(7, 0).getPiece().equals(queen);
    }

    @Test
    public void queenGetTargetsTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(4, 4);
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen, initialCell);

        Cell targetCell1 = board.getCellByCoordinates(2, 4);
        Knight knight1 = new Knight(Color.BLACK);
        board.putPiece(knight1, targetCell1);

        Cell targetCell2 = board.getCellByCoordinates(7, 4);
        Knight knight2 = new Knight(Color.BLACK);
        board.putPiece(knight2, targetCell2);

        Cell targetCell3 = board.getCellByCoordinates(1, 1);
        Knight knight3 = new Knight(Color.BLACK);
        board.putPiece(knight3, targetCell3);

        Cell targetCell4 = board.getCellByCoordinates(4, 1);
        Knight knight4 = new Knight(Color.WHITE);
        board.putPiece(knight4, targetCell4);

        Set<Cell> queenTargets = queen.getTargets();

        System.out.println(board);
        System.out.println(queenTargets);

        Set<Cell> correctTargets = new HashSet<Cell>();
        correctTargets.add(targetCell1);
        correctTargets.add(targetCell2);
        correctTargets.add(targetCell3);

        assert queenTargets.equals(correctTargets);
    }

    @Test
    public void queenCaptureTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(4, 4);
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen, initialCell);

        Cell targetCell1 = board.getCellByCoordinates(2, 4);
        Knight knight1 = new Knight(Color.BLACK);
        board.putPiece(knight1, targetCell1);

        Cell targetCell2 = board.getCellByCoordinates(7, 4);
        Knight knight2 = new Knight(Color.BLACK);
        board.putPiece(knight2, targetCell2);

        Cell targetCell3 = board.getCellByCoordinates(1, 1);
        Knight knight3 = new Knight(Color.BLACK);
        board.putPiece(knight3, targetCell3);

        Cell targetCell4 = board.getCellByCoordinates(4, 1);
        Knight knight4 = new Knight(Color.WHITE);
        board.putPiece(knight4, targetCell4);

        System.out.println(board);
        assert board.getPieces().contains(knight1);
        assert board.getPieces().contains(knight3);
        assert targetCell1.getPiece().equals(knight1);
        assert targetCell3.getPiece().equals(knight3);

        queen.capture(targetCell3);
        queen.move(board.getCellByCoordinates(0, 2));
        queen.capture(targetCell1);

        System.out.println(board);
        assert !board.getPieces().contains(knight1);
        assert !board.getPieces().contains(knight3);
        assert targetCell3.isEmpty();
        assert targetCell1.getPiece().equals(queen);
    }
}
