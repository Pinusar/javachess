import chessEngine.*;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class RookTests {
    @Test
    public void rookGetMovesTest() {
        Board board = new Board();
        Cell targetCell = new Cell(0, 0, board);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, targetCell);

        Cell bishopCell = new Cell(0, 2);
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, bishopCell);

        Cell knightCell = new Cell(3, 0);
        Knight knight = new Knight(Color.BLACK);
        board.putPiece(knight, knightCell);

        Set<Cell> rookMoves = rook.getMoves();
        System.out.println(rookMoves);
        System.out.println(board);
        Set<Cell> moves = new HashSet<>();
        Cell move1 = new Cell(0, 1, board);
        Cell move2 = new Cell(1, 0, board);
        Cell move3 = new Cell(2, 0, board);
        moves.add(move1);
        moves.add(move2);
        moves.add(move3);
        assert rookMoves.equals(moves);
    }

    @Test
    public void rookMoveTest() {
        Board board = new Board();
        Cell initialCell = new Cell(0, 0, board);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCells()[7][0];
        rook.move(targetCell);
        System.out.println(board);
        assert board.getPieces().contains(rook);
        assert board.getCells()[0][0].isEmpty();
        assert board.getCells()[7][0].getPiece().equals(rook);
    }

    @Test
    public void rookGetTargetsTest() {
        Board board = new Board();
        Cell initialCell = new Cell(4, 4, board);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, initialCell);

        Cell targetCell1 = new Cell(2, 4, board);
        Knight knight1 = new Knight(Color.BLACK);
        board.putPiece(knight1,targetCell1);

        Cell targetCell2 = new Cell(7, 4, board);
        Knight knight2 = new Knight(Color.BLACK);
        board.putPiece(knight2,targetCell2);

        Cell targetCell3 = new Cell(4, 1, board);
        Knight knight3 = new Knight(Color.WHITE);
        board.putPiece(knight3,targetCell3);

        Set<Cell> rookTargets = rook.getTargets();

        System.out.println(board);
        System.out.println(rookTargets);

        Set<Cell> correctTargets = new HashSet<>();
        correctTargets.add(targetCell1);
        correctTargets.add(targetCell2);

        assert rookTargets.equals(correctTargets);
    }

    @Test
    public void rookCaptureTest() {
        Board board = new Board();

        Cell initialCell = board.getCellByCoordinates(0, 2);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, initialCell);

        Cell knightCell = board.getCellByCoordinates(4, 2);
        Knight knight = new Knight(Color.BLACK);
        board.putPiece(knight,knightCell);

        System.out.println(board);
        assert board.getPieces().contains(knight);
        assert knightCell.getPiece().equals(knight);

        rook.capture(knightCell);
        System.out.println(board);

        assert !board.getPieces().contains(knight);
        assert knightCell.getPiece().equals(rook);
    }
}
