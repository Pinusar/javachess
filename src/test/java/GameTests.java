import chessEngine.*;
import org.junit.Test;

public class GameTests {
    @Test
    public void playTurnTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        System.out.println(game.getBoard());
        game.playTurn("a2 a4");
        System.out.println(game.getBoard());
        assert board.getCellByCoordinates(1, 0).isEmpty();
        assert !board.getCellByCoordinates(3, 0).isEmpty();
    }

    @Test
    public void simualtionTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        System.out.println(game.getBoard());
        game.playTurn("a2 a4");
        System.out.println(game.getBoard());
        Board simulation = game.createSimulationBoard();
        System.out.println(simulation);
        game.playTurn("e7 e5");
        System.out.println(game.getBoard());
        System.out.println(simulation);
        Game sim = game.createSimulation();
        sim.playTurn("a4 a5");
        System.out.println(sim.getBoard());
        System.out.println(game.getBoard());
    }

    @Test
    public void moveWouldPutPlayerUnderCheckTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        game.playTurn("e2 e4");
        game.playTurn("e7 e5");
        game.playTurn("f1 b5");
        System.out.println(board);
        assert game.moveWouldPutActivePlayerUnderCheck("d7 d6");
    }

    @Test
    public void findCastlingTargetRookTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        game.playTurn("e2 e4");
        game.playTurn("e7 e5");
        game.playTurn("f1 b5");
        game.playTurn("a7 a6");
        game.playTurn("g1 f3");
        System.out.println(board);
        Cell kingCell = board.getCellByCoordinates(0, 4);
        King king = (King) board.getPieceByCell(kingCell);
        Cell castlingTargetCell = board.getCellByCoordinates(0, 6);
        Rook targetRook = game.findCastlingTargetRook(king, castlingTargetCell);
        Cell rookCell = board.getCellByCoordinates(0, 7);
        Piece rook = board.getPieceByCell(rookCell);
        System.out.println(targetRook);
        assert rook.equals(targetRook);
    }

    @Test
    public void castleKingSideTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        game.playTurn("e2 e4");
        game.playTurn("e7 e5");
        game.playTurn("f1 b5");
        game.playTurn("a7 a6");
        game.playTurn("g1 f3");
        game.playTurn("a6 a5");
        game.playTurn("e1 g1");
        System.out.println(board);
    }

    @Test
    public void playerCanEscapeCheckWhenTrueTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("f1 b5");
        System.out.println(board);
        assert game.activePlayerCanEscapeCheck();
    }

    @Test
    public void playerCanEscapeCheckWhenFalseTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        game.playTurn("e2 e4");
        game.playTurn("e7 e5");
        game.playTurn("f1 c4");
        game.playTurn("a7 a6");
        game.playTurn("d1 f3");
        game.playTurn("a6 a5");
        game.playTurn("f3 f7");
        System.out.println(board);
        assert !game.activePlayerCanEscapeCheck();
    }

    @Test
    public void getActivePlayerMaterialTotalTest() {
        Board board = new Board();
        Player player1 = new Player(Color.WHITE, board);
        Player player2 = new Player(Color.BLACK, board);
        Game game = new Game(player1, player2, board);
        game.getBoard().initializePieces();
        System.out.println(game.getBoard());
        game.playTurn("e2 e4");
        System.out.println(game.getBoard());
        game.playTurn("d7 d5");
        System.out.println(game.getBoard());
        game.playTurn("e4 d5");
        System.out.println(game.getBoard());
        System.out.println(game.getActivePlayerMaterialScore());
        System.out.println(game.getActivePlayerMaterialDifference());
    }

    @Test
    public void moveAfterCheckTest() {
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        Game game = new Game(whitePlayer, blackPlayer, board);
        game.getBoard().initializePieces();
        System.out.println(board);
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("f1 b5");
        game.playTurn("c7 c6");
        System.out.println(board);

    }
}
