import chessEngine.*;
import org.junit.Test;

public class BoardTests {


    @Test
    public void boardKnightReprTest() {
        Board board = new Board();
        Cell initialCell = new Cell(0, 1, board);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, initialCell);
        System.out.println(board);
        System.out.println(knight.getPosition());
    }

    @Test
    public void putPieceTest() {
        Board board = new Board();
        Cell initialCell = new Cell(0, 1, board);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, initialCell);
        assert board.getPieces().contains(knight);
    }

    @Test
    public void clearCellTest() {
        Board board = new Board();
        Cell initialCell = new Cell(0, 1, board);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, initialCell);
        board.removePiece(initialCell);
        assert !board.getPieces().contains(knight);
    }

    @Test
    public void removeAllPiecesTest() {
        Board board = new Board();
        Cell initialCell = board.getCellByCoordinates(0, 1);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, initialCell);

        Cell initialCell2 = board.getCellByCoordinates(2, 3);
        Knight knight2 = new Knight(Color.WHITE);
        board.putPiece(knight2, initialCell2);

        Cell initialCell3 = board.getCellByCoordinates(5, 7);
        Knight knight3 = new Knight(Color.WHITE);
        board.putPiece(knight3, initialCell3);

        System.out.println(board);
        board.removeAllPieces();
        System.out.println(board);
        assert board.getPieces().size() == 0;
    }

    @Test
    public void initializePiecesTest() {
        Board board = new Board();
        board.initializePieces();
        System.out.println(board);
    }

    @Test
    public void getMaterialValueByColorTest() {
        Board board = new Board();
        Cell knightCell = board.getCellByCoordinates(0, 1);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, knightCell);
        Cell rookCell = board.getCellByCoordinates(0, 0);
        Rook rook = new Rook(Color.WHITE);
        board.putPiece(rook, rookCell);
        Cell queenCell = board.getCellByCoordinates(0, 3);
        Queen queen = new Queen(Color.WHITE);
        board.putPiece(queen, queenCell);
        assert board.getMaterialValueByColor(Color.WHITE) == 17;
    }

}
