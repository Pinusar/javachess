import chessEngine.*;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class BishopTests {
    @Test
    public void bishopGetMovesTest() {
        Board board = new Board();
        Cell targetCell = new Cell(0, 2, board);
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, targetCell);
        Set<Cell> bishopMoves = bishop.getMoves();
        System.out.println(bishopMoves);
        System.out.println(board);
        Set<Cell> moves = new HashSet<>();
        Cell move1 = new Cell(1, 1, board);
        Cell move2 = new Cell(2, 0, board);
        Cell move3 = new Cell(1, 3, board);
        Cell move4 = new Cell(2, 4, board);
        Cell move5 = new Cell(3, 5, board);
        Cell move6 = new Cell(4, 6, board);
        Cell move7 = new Cell(5, 7, board);
        moves.add(move1);
        moves.add(move2);
        moves.add(move3);
        moves.add(move4);
        moves.add(move5);
        moves.add(move6);
        moves.add(move7);
        assert bishopMoves.equals(moves);
    }

    @Test
    public void bishopMoveTest() {
        Board board = new Board();
        Cell initialCell = new Cell(0, 2, board);
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCells()[5][7];
        bishop.move(targetCell);
        System.out.println(board);
        assert board.getPieces().contains(bishop);
    }

    @Test
    public void bishopGetTargetsTest() {
        Board board = new Board();
        Cell initialCell = new Cell(4, 4, board);
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, initialCell);

        Cell targetCell1 = new Cell(2, 2, board);
        Knight knight1 = new Knight(Color.BLACK);
        board.putPiece(knight1,targetCell1);

        Cell targetCell2 = new Cell(7, 7, board);
        Knight knight2 = new Knight(Color.BLACK);
        board.putPiece(knight2,targetCell2);

        Cell targetCell3 = new Cell(2, 6, board);
        Knight knight3 = new Knight(Color.WHITE);
        board.putPiece(knight3,targetCell3);

        Set<Cell> bishopTargets = bishop.getTargets();

        System.out.println(board);
        System.out.println(bishopTargets);

        Set<Cell> correctTargets = new HashSet<>();
        correctTargets.add(targetCell1);
        correctTargets.add(targetCell2);

        assert bishopTargets.equals(correctTargets);
    }

    @Test
    public void bishopCaptureTest() {
        Board board = new Board();

        Cell initialCell = board.getCellByCoordinates(0, 2);
        Bishop bishop = new Bishop(Color.WHITE);
        board.putPiece(bishop, initialCell);

        Cell knightCell = board.getCellByCoordinates(2, 4);
        Knight knight = new Knight(Color.BLACK);
        board.putPiece(knight,knightCell);

        System.out.println(board);
        assert board.getPieces().contains(knight);
        assert knightCell.getPiece().equals(knight);

        bishop.capture(board.getCellByCoordinates(2, 4));
        System.out.println(board);

        assert !board.getPieces().contains(knight);
        assert knightCell.getPiece().equals(bishop);
    }
}
