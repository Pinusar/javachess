import chessEngine.Board;
import chessEngine.Cell;
import chessEngine.Color;
import chessEngine.Knight;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class KnightTests {

    @Test
    public void knightGetMovesTest() {
        Board board = new Board();
        Cell targetCell = new Cell(0, 1, board);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, targetCell);
        Set<Cell> knightMoves = knight.getMoves();
        Set<Cell> moves = new HashSet<>();
        Cell move1 = new Cell(2, 0, board);
        Cell move2 = new Cell(2, 2, board);
        Cell move3 = new Cell(1, 3, board);
        moves.add(move1);
        moves.add(move2);
        moves.add(move3);
        assert knightMoves.equals(moves);
    }

    @Test
    public void moveTest() {
        Board board = new Board();
        Cell initialCell = new Cell(0, 1, board);
        Knight knight = new Knight(Color.WHITE);
        board.putPiece(knight, initialCell);
        System.out.println(board);
        Cell targetCell = board.getCells()[2][0];
        knight.move(targetCell);
        System.out.println(board);
        assert board.getPieces().contains(knight);
    }

    @Test
    public void captureTest() {
        Board board = new Board();
        Cell whiteKnightCell = new Cell(0, 1, board);
        Knight whiteKnight = new Knight(Color.WHITE);
        board.putPiece(whiteKnight, whiteKnightCell);
        Cell blackKnightCell = new Cell(2, 0, board);
        Knight blackKnight = new Knight(Color.BLACK);
        board.putPiece(blackKnight, blackKnightCell);
        System.out.println(board);
        Cell targetCell = board.getCells()[2][0];
        assert targetCell.getPiece() == blackKnight;
        assert board.getPieces().contains(blackKnight);
        whiteKnight.capture(targetCell);
        System.out.println(board);
        assert targetCell.getPiece() == whiteKnight;
        assert !board.getPieces().contains(blackKnight);
    }
}
