import chessEngine.*;
import org.junit.Test;

import java.util.List;

public class AiTests {
    @Test
    public void getMoveBasicTest() {
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        Game game = new Game(whitePlayer, blackPlayer, board);
        game.getBoard().initializePieces();
        System.out.println(board);
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("d1 g4");
        System.out.println(board);

        Ai ai = new Ai(game);
        Move bestMove = ai.getNextMove(Color.BLACK, game);
        assert bestMove.getMoveString().equals("c8 g4");
    }

    @Test
    public void getMoveTwoTurnsAheadTest() {
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        Game game = new Game(whitePlayer, blackPlayer, board);
        game.getBoard().initializePieces();
        System.out.println(board);
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("d1 f3");
        game.playTurn("g8 f6");
        System.out.println(board);

        Ai ai = new Ai(game);
        Move bestMove = ai.getMoveTwoTurnsAhead(Color.WHITE, game);
        System.out.println(bestMove.toString());
        assert !bestMove.getMoveString().equals("f3 f6");
    }

    @Test
    public void getTwoTurnsMoveListTest() {
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        Game game = new Game(whitePlayer, blackPlayer, board);
        game.getBoard().initializePieces();
        System.out.println(board);
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("d1 f3");
        game.playTurn("g8 f6");
        System.out.println(board);

        Ai ai = new Ai(game);
        List<Move> bestMoves = ai.getTwoTurnMoveList(Color.WHITE, game);
        System.out.println(bestMoves);
        assert bestMoves.size() == 2;
    }

    @Test
    public void getThreeTurnsMoveListTest() {
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        Game game = new Game(whitePlayer, blackPlayer, board);
        game.getBoard().initializePieces();
        System.out.println(board);
        game.playTurn("e2 e4");
        game.playTurn("d7 d5");
        game.playTurn("d1 f3");
        game.playTurn("g8 f6");
        System.out.println(board);

        Ai ai = new Ai(game);
        List<Move> bestMoves = ai.getThreeTurnsMovesList(Color.WHITE, game);
        System.out.println(bestMoves);
        //assert bestMoves.size() == 2;
    }
}
