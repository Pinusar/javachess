import chessEngine.*;
import org.junit.Test;

import java.util.List;

public class PlayerTests {
    @Test
    public void getPiecesTest() {
        Board board = new Board();
        board.initializePieces();
        Player player = new Player(Color.BLACK, board);
        System.out.println(board);
        List<Piece> playerPieces = player.getPieces();
        assert playerPieces.size() == 16;
        for (Piece piece: playerPieces
             ) {
            assert piece.getColor() == Color.BLACK;
        }
    }

    @Test
    public void getKingTest() {
        Board board = new Board();
        board.initializePieces();
        Player player = new Player(Color.BLACK, board);
        Piece blackKing = board.getPieceByCell(board.getCellByCoordinates(7, 4));
        System.out.println(board);
        assert player.getKing().equals(blackKing);
    }

    @Test
    public void playerUnderCheckTest() {
        Board board = new Board();
        Player player = new Player(Color.BLACK, board);
        King blackKing = new King(Color.BLACK);
        board.putPiece(blackKing, board.getCellByCoordinates(7, 4));
        Rook whiteRook = new Rook(Color.WHITE);
        board.putPiece(whiteRook, board.getCellByCoordinates(7, 0));
        System.out.println(board);
        assert player.isUnderCheck();
    }

    @Test
    public void playerNotUnderCheckTest() {
        Board board = new Board();
        Player player = new Player(Color.BLACK, board);
        King blackKing = new King(Color.BLACK);
        board.putPiece(blackKing, board.getCellByCoordinates(7, 4));
        Rook whiteRook = new Rook(Color.WHITE);
        board.putPiece(whiteRook, board.getCellByCoordinates(6, 0));
        System.out.println(board);
        assert !player.isUnderCheck();
    }
}
