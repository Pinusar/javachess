import com.twilio.twiml.MessagingResponse;
import com.twilio.twiml.messaging.Body;
import com.twilio.twiml.messaging.Message;
import chessEngine.Board;
import chessEngine.Color;
import chessEngine.Game;
import chessEngine.Player;

import static spark.Spark.*;

public class SmsApp {
    public static void main(String[] args) {

        String player1Number = "+3725028206";
        String player2Number = "+37253835757";

        String portEnv = System.getenv("PORT");
        if (portEnv == null || portEnv == "") {
            portEnv = "4567";
        }
        int port = Integer.parseInt(portEnv);
        port(port);

        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        board.initializePieces();
        Game game = new Game(whitePlayer, blackPlayer, board);


        post("/play", (req, res) -> {
            String responseNumber;
            res.type("application/xml");
            String input = req.queryParams("Body").toLowerCase();
            String senderNumber = req.queryParams("From");
            if (senderNumber.equals(player1Number)) {
                responseNumber = player2Number;
            } else {
                responseNumber = player1Number;
            }
            Body body = new Body
                    .Builder("")
                    .build();
            if (input.equals("new game")) {
                game.startNewGame();
                body = new Body
                        .Builder(board.toString())
                        .build();
                Message sms = new Message
                        .Builder()
                        .body(body)
                        .to(responseNumber)
                        .build();
                MessagingResponse twiml = new MessagingResponse
                        .Builder()
                        .message(sms)
                        .build();
                return twiml.toXml();
            } else {
                try {
                    game.playTurn(input);
                    body = new Body
                            .Builder(board.toString())
                            .build();
                } catch (Exception e) {
                    body = new Body
                            .Builder("Invalid move!")
                            .build();
                }
                Message sms = new Message
                        .Builder()
                        .body(body)
                        .to(responseNumber)
                        .build();
                MessagingResponse twiml = new MessagingResponse
                        .Builder()
                        .message(sms)
                        .build();
                return twiml.toXml();
            }
        });
    }
}