import chessEngine.*;

import java.util.ArrayList;
import java.util.List;

public class Ai {
    private Game game;

    public Ai(Game game) {
        this.game = game;
    }


    public Game getGame() {
        return game;
    }

    public Move getNextMove(Color playerColor, Game game1) {
        int bestMaterialValue = -1000;
        Move bestMove = null;
        Player currentPlayer = game1.getPlayerByColor(playerColor);
        for (Piece piece : currentPlayer.getPieces()
        ) {
            Cell startingCell = piece.getPosition();
            for (Cell targetCell : piece.getMovesAndTargets()
            ) {
                int materialValueOfMove = getMaterialValueOfPotentialMove(startingCell, targetCell, game1);
                if (materialValueOfMove > bestMaterialValue) {
                    bestMaterialValue = materialValueOfMove;
                    bestMove = new Move(startingCell, targetCell, game1.getBoard());
                }
            }
        }
        return bestMove;
    }

    public Move getMoveTwoTurnsAhead(Color playerColor, Game game1) {
        List<Move> twoTurnMoveList = getTwoTurnMoveList(playerColor, game1);
        Move nextMove = twoTurnMoveList.get(0);
        return nextMove;
    }


    public List<Move> getTwoTurnMoveList(Color playerColor, Game game1) {
        List<Move> movesList = new ArrayList<>();
        int bestMaterialValue = -1000;
        Move bestMove = null;
        Player currentPlayer = game1.getPlayerByColor(playerColor);
        Color currentPlayerColor = currentPlayer.getColor();
        for (Piece piece : currentPlayer.getPieces()
        ) {
            Cell startingCell = piece.getPosition();
            for (Cell targetCell : piece.getMovesAndTargets()
            ) {
                Game simulation = game1.createSimulation();
                Cell simulationStartingCell = simulation.getBoard().getCellByCoordinates(startingCell.getX(), startingCell.getY());
                Cell simulationTargetCell = simulation.getBoard().getCellByCoordinates(targetCell.getX(), targetCell.getY());

                simulation.playTurn(simulationStartingCell, simulationTargetCell);
                currentPlayerColor = currentPlayerColor.getOpposite();
                Move bestNextMove = getNextMove(currentPlayerColor, simulation);
                simulation.playTurn(bestNextMove);
                currentPlayerColor = currentPlayerColor.getOpposite();
                int materialValueOfMove = simulation.getPlayerMaterialDifferenceByColor(playerColor);
                if (materialValueOfMove > bestMaterialValue) {
                    bestMaterialValue = materialValueOfMove;
                    movesList.clear();
                    bestMove = new Move(startingCell, targetCell, game1.getBoard());
                    movesList.add(bestMove);
                    movesList.add(bestNextMove);
                }
            }
        }
        return movesList;
    }

    public List<Move> getThreeTurnsMovesList(Color playerColor, Game game1) {
        List<Move> movesList = new ArrayList<>();
        int bestMaterialValue = -1000;
        Move bestMove = null;
        Player currentPlayer = game1.getPlayerByColor(playerColor);
        Color currentPlayerColor = currentPlayer.getColor();
        for (Piece piece : currentPlayer.getPieces()
        ) {
            Cell startingCell = piece.getPosition();
            for (Cell targetCell : piece.getMovesAndTargets()
            ) {
                Game simulation = game1.createSimulation();
                Cell simulationStartingCell = simulation.getBoard().getCellByCoordinates(startingCell.getX(), startingCell.getY());
                Cell simulationTargetCell = simulation.getBoard().getCellByCoordinates(targetCell.getX(), targetCell.getY());

                simulation.playTurn(simulationStartingCell, simulationTargetCell);
                System.out.println(simulation.getBoard());
                currentPlayerColor = currentPlayerColor.getOpposite();

                List<Move> nextMoves = getTwoTurnMoveList(currentPlayerColor, simulation);
                System.out.println("==========ANALYZING NEXT MOVES=================");
                for (Move nextMove: nextMoves
                     ) {
                    simulation.playTurn(nextMove);
                    currentPlayerColor = currentPlayerColor.getOpposite();
                }

                currentPlayerColor = currentPlayerColor.getOpposite();

                int materialValueOfMove = simulation.getPlayerMaterialDifferenceByColor(playerColor);
                if (materialValueOfMove > bestMaterialValue) {
                    bestMaterialValue = materialValueOfMove;
                    movesList.clear();
                    bestMove = new Move(startingCell, targetCell, game1.getBoard());
                    movesList.add(bestMove);
                    movesList.addAll(nextMoves);
                }
            }
        }
        return movesList;
    }


    public int getMaterialValueOfPotentialMove(Cell startingCell, Cell targetCell, Game game) {
        int materialValueOfPosition = -1000;
        Game simulation = game.createSimulation();
        Cell simulationStartingCell = simulation.getBoard().getCellByCoordinates(startingCell.getX(), startingCell.getY());
        Cell simulationTargetCell = simulation.getBoard().getCellByCoordinates(targetCell.getX(), targetCell.getY());
        try {
            simulation.playTurn(simulationStartingCell, simulationTargetCell);
            materialValueOfPosition = simulation.getInActivePlayerMaterialDifference();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
        return materialValueOfPosition;
    }
}
