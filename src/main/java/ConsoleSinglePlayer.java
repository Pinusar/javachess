import chessEngine.*;

import java.util.Scanner;

public class ConsoleSinglePlayer {
    public void play() {
        System.out.println("Starting single player game");
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        board.initializePieces();
        Game game = new Game(whitePlayer, blackPlayer, board);
        boolean running = true;
        Scanner inputScanner = new Scanner(System.in);
        System.out.println(game.getBoard());
        Ai ai = new Ai(game);
        while (running) {
            if (game.isOver()) {
                System.out.println("models.Game over!");
                break;
            }
            System.out.println("Please enter move: ");
            String move = inputScanner.nextLine();
            if (move.equals("q")) {
                break;
            }
            try {
                game.playTurn(move);
                System.out.println(String.format("Your move: %s", move));
                System.out.println(game.getBoard());
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid move.");
                continue;
            }
            Move computerMove = ai.getMoveTwoTurnsAhead(Color.BLACK, game);
            System.out.println(computerMove);
            System.out.println("Executing computer move...");
            System.out.println(computerMove.getMoveString());
            game.playTurn(computerMove.getMoveString());
            System.out.println(game.getBoard());
        }
        System.out.println(game.getStatusMessage());
        System.out.println("models.Move history:");
        for (String move: game.getMovesHistory()
        ) {
            System.out.println(move);
        }
    }
}
