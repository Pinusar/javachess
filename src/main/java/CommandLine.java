import java.util.Scanner;

public class CommandLine {
    public static void main(String[] args) {
        System.out.println("Starting game. Please choose mode:");
        System.out.println("1. Single player");
        System.out.println("2. Multiplayer");

        String choice = "";
        Scanner inputScanner = new Scanner(System.in);

        while (!choice.equals("1") && !choice.equals("2")) {
            choice = inputScanner.nextLine();
            if (choice.equals("1")) {
                ConsoleSinglePlayer singlePlayerGame = new ConsoleSinglePlayer();
                singlePlayerGame.play();
                break;
            } else if (choice.equals("2")) {
                ConsoleMultiplayer multiplayerGame = new ConsoleMultiplayer();
                multiplayerGame.play();
                break;
            }
        }
    }
}
