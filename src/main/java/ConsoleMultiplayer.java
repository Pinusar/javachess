import chessEngine.Board;
import chessEngine.Color;
import chessEngine.Game;
import chessEngine.Player;

import java.util.Scanner;

public class ConsoleMultiplayer {
    public void play() {
        System.out.println("Starting multiplayer game");
        Board board = new Board();
        Player whitePlayer = new Player(Color.WHITE, board);
        Player blackPlayer = new Player(Color.BLACK, board);
        board.initializePieces();
        Game game = new Game(whitePlayer, blackPlayer, board);
        boolean running = true;
        Scanner inputScanner = new Scanner(System.in);
        System.out.println(game.getBoard());
        while (running) {
            if (game.isOver()) {
                System.out.println("Game over!");
                break;
            }
            System.out.println("Please enter move: ");
            String move = inputScanner.nextLine();
            if (move.equals("q")) {
                break;
            }
            try {
                game.playTurn(move);
                System.out.println(game.getBoard());
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid move.");
            }
        }
        System.out.println(game.getStatusMessage());
        System.out.println("Move history:");
        for (String move: game.getMovesHistory()
        ) {
            System.out.println(move);
        }
    }
}
