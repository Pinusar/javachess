package chessEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class Game {
    private Player whitePlayer;
    private Player blackBlayer;
    private Player activePlayer;
    private Board board;
    private int turnCount;
    private List<String> movesHistory;
    private boolean over;
    private String statusMessage;


    public Game(Player whitePlayer, Player blackBlayer, Board board) {
        this.whitePlayer = whitePlayer;
        this.blackBlayer = blackBlayer;
        this.activePlayer = whitePlayer;
        this.board = board;
        this.turnCount = 1;
        this.movesHistory = new ArrayList<>();
        this.over = false;
        this.statusMessage = "Game is ongoing";
    }

    public Player getWhitePlayer() {
        return whitePlayer;
    }

    public Player getBlackBlayer() {
        return blackBlayer;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public Board getBoard() {
        return board;
    }

    public int getTurnCount() {
        return turnCount;
    }

    public List<String> getMovesHistory() {
        return movesHistory;
    }

    public boolean isOver() {
        return over;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }

    public Player getPlayerByColor(Color color) {
        if (color == Color.WHITE) {
            return whitePlayer;
        } else {
            return blackBlayer;
        }
    }

    public void playTurn(String moveString) {
        //TODO: add regex checking for input
        Move move = new Move(moveString, this.board);
        Cell startingCell = move.getStartingCell();
        Cell targetCell = move.getTargetCell();
        playTurn(startingCell, targetCell);
    }

    public void playTurn(Cell startingCell, Cell targetCell) {
        Cell newStartingCell = getBoard().getCellByCoordinates(startingCell.getX(), startingCell.getY());
        Cell newTargetCell = getBoard().getCellByCoordinates(targetCell.getX(), targetCell.getY());

        playTurnWithoutCheckingForGameOver(newStartingCell, newTargetCell);
        if (activePlayer.isUnderCheck() && !activePlayerCanEscapeCheck()) {
            this.over = true;
            this.statusMessage = String.format("Game over. %s player lost.", activePlayer.getColor());
        }
    }

    public void playTurnWithoutCheckingForGameOver(Cell startingCell, Cell targetCell) {
        Move move = new Move(startingCell, targetCell, this.board);
        reducePawnsDoubleMoveTimer();
        if (startingCell.isEmpty()) {
            throw new IllegalArgumentException("Invalid move! Starting cell is empty!");
        } else {
            Piece piece = board.getPieceByCell(startingCell);
            if (piece.getColor() == activePlayer.getColor()) {
                if (moveWouldPutActivePlayerUnderCheck(move.getMoveString())) {
                    throw new IllegalArgumentException("Move would put player under check!");
                }
                if (piece instanceof King && Math.abs(targetCell.getY() - piece.getPosition().getY()) > 1) {
                    King king = (King) piece;
                    Rook targetRook = findCastlingTargetRook(king, targetCell);
                    king.castle(targetRook);
                } else {
                    piece.moveOrCapture(targetCell);
                }
            } else {
                throw new IllegalArgumentException("It is other player's turn!");
            }
        }
        movesHistory.add(move.getMoveString());
        if (activePlayer == blackBlayer) {
            turnCount++;
        }
        switchActivePlayer();
    }

    public void playTurnWithoutCheckingForCheck(Cell startingCell, Cell targetCell) {
        Move move = new Move(startingCell, targetCell, this.board);
        if (startingCell.isEmpty()) {
            throw new IllegalArgumentException("Invalid move! Starting cell is empty!");
        } else {
            Piece piece = board.getPieceByCell(startingCell);
            if (piece.getColor() == activePlayer.getColor()) {
                if (piece instanceof King && Math.abs(targetCell.getY() - piece.getPosition().getY()) > 1) {
                    King king = (King) piece;
                    Rook targetRook = findCastlingTargetRook(king, targetCell);
                    king.castle(targetRook);
                } else {
                    piece.moveOrCapture(targetCell);
                }
            } else {
                throw new IllegalArgumentException("It is other player's turn!");
            }
        }
        movesHistory.add(move.getMoveString());
        if (activePlayer == blackBlayer) {
            turnCount++;
        }
        switchActivePlayer();
    }

    public void playTurn(Move move) {
        Cell startingCell = move.getStartingCell();
        Cell targetCell = move.getTargetCell();
        playTurn(startingCell, targetCell);
    }

    private void reducePawnsDoubleMoveTimer() {
        List<Piece> pawns = this.board.getPawns();
        for (Piece piece : pawns
        ) {
            Pawn pawn = (Pawn) piece;
            if (pawn.getMovedDoubleTimer() > 0) {
                pawn.reduceDoubleMoveTimer();
            }
        }
    }

    public void switchActivePlayer() {
        if (activePlayer == whitePlayer) {
            activePlayer = blackBlayer;
        } else {
            activePlayer = whitePlayer;
        }
    }


    public boolean moveWouldPutActivePlayerUnderCheck(String moveString) {
        Game simulation = createSimulation();
        Move move = new Move(moveString, simulation.getBoard());
        Player activePlayer = simulation.getActivePlayer();
        simulation.playTurnWithoutCheckingForCheck(move.getStartingCell(), move.getTargetCell());
        if (activePlayer.isUnderCheck()) {
            return true;
        } else {
            return false;
        }
    }

    public Board createSimulationBoard() {
        Board newBoard = new Board();
        for (Cell[] row : this.board.getCells()
        ) {
            for (Cell cell : row
            ) {
                if (!cell.isEmpty()) {
                    Piece currentPiece = cell.getPiece();
                    if (currentPiece instanceof Pawn) {
                        Pawn currentPawn = (Pawn) currentPiece;
                        Pawn newPiece = new Pawn(currentPiece.color);
                        newPiece.setMovedDoubleTimer(currentPawn.getMovedDoubleTimer());
                        newBoard.putPiece(newPiece, newBoard.getCellByCoordinates(currentPiece.getPosition().getX(), currentPiece.getPosition().getY()));
                    } else if (currentPiece instanceof Knight) {
                        Knight newPiece = new Knight(currentPiece.color);
                        newBoard.putPiece(newPiece, newBoard.getCellByCoordinates(currentPiece.getPosition().getX(), currentPiece.getPosition().getY()));
                    } else if (currentPiece instanceof Bishop) {
                        Bishop newPiece = new Bishop(currentPiece.color);
                        newBoard.putPiece(newPiece, newBoard.getCellByCoordinates(currentPiece.getPosition().getX(), currentPiece.getPosition().getY()));
                    } else if (currentPiece instanceof Rook) {
                        Rook currentRook = (Rook) currentPiece;
                        Rook newPiece = new Rook(currentPiece.color);
                        newPiece.setMoved(currentRook.hasMoved());
                        newBoard.putPiece(newPiece, newBoard.getCellByCoordinates(currentPiece.getPosition().getX(), currentPiece.getPosition().getY()));
                    } else if (currentPiece instanceof Queen) {
                        Queen newPiece = new Queen(currentPiece.color);
                        newBoard.putPiece(newPiece, newBoard.getCellByCoordinates(currentPiece.getPosition().getX(), currentPiece.getPosition().getY()));
                    } else if (currentPiece instanceof King) {
                        King currentKing = (King) currentPiece;
                        King newPiece = new King(currentPiece.color);
                        newPiece.setMoved(currentKing.hasMoved());
                        newBoard.putPiece(newPiece, newBoard.getCellByCoordinates(currentPiece.getPosition().getX(), currentPiece.getPosition().getY()));
                    }
                }
            }

        }
        return newBoard;
    }

    public Game createSimulation() {
        Board simulationBoard = createSimulationBoard();
        Player simulationWhitePlayer = new Player(Color.WHITE, simulationBoard);
        Player simulationBlackPlayer = new Player(Color.BLACK, simulationBoard);
        Game simulation = new Game(simulationWhitePlayer, simulationBlackPlayer, simulationBoard);
        if (this.activePlayer == this.whitePlayer) {
            simulation.setActivePlayer(simulationWhitePlayer);
        } else {
            simulation.setActivePlayer(simulationBlackPlayer);
        }
        return simulation;
    }

    public Rook findCastlingTargetRook(King king, Cell targetCell) {
        Rook targetRook = null;
        if (king.getPosition().getX() == 0) {
            if (targetCell.getY() == 6) {
                targetRook = board.findRookByCoordinates(0, 7);
            } else if (targetCell.getY() == 2) {
                targetRook = board.findRookByCoordinates(0, 0);
            } else {
                throw new IllegalArgumentException("Illegal castling attempt");
            }
        } else if (king.getPosition().getX() == 7) {
            if (targetCell.getY() == 6) {
                targetRook = board.findRookByCoordinates(7, 7);
            } else if (targetCell.getY() == 2) {
                targetRook = board.findRookByCoordinates(7, 0);
            } else {
                throw new IllegalArgumentException("Illegal castling attempt");
            }
        } else {
            throw new IllegalArgumentException("Illegal castling attempt");
        }
        return targetRook;
    }

    public int getNumberOfAvailableMoves() {
        int availableMoves = 0;
        List<Piece> activePlayerPieces = getActivePlayerPieces();
        for (Piece piece: activePlayerPieces
             ) {
            availableMoves += piece.getMoves().size();
        }
        return availableMoves;
    }

    private List<Piece> getActivePlayerPieces() {
        Player activePlayer = getActivePlayer();
        return board.getPiecesByColor(activePlayer.getColor());
    }

    public int getActivePlayerMaterialScore() {
        Player activePlayer = getActivePlayer();
        return board.getMaterialValueByColor(activePlayer.getColor());
    }

    public int getInactivePlayerMaterialScore() {
        Color inactiveColor = null;
        if (getActivePlayer().getColor() == Color.WHITE) {
            inactiveColor = Color.BLACK;
        } else {
            inactiveColor = Color.WHITE;
        }
        return board.getMaterialValueByColor(inactiveColor);
    }

    public int getActivePlayerMaterialDifference() {
        return getActivePlayerMaterialScore() - getInactivePlayerMaterialScore();
    }

    public int getInActivePlayerMaterialDifference() {
        return getInactivePlayerMaterialScore() - getActivePlayerMaterialScore();
    }

    public int getPlayerMaterialDifferenceByColor(Color color) {
        if (activePlayer.getColor() == color) {
            return getActivePlayerMaterialDifference();
        } else {
            return getInActivePlayerMaterialDifference();
        }
    }

    public boolean activePlayerCanEscapeCheck() {
        if (!activePlayer.isUnderCheck()) {
            throw new RuntimeException("Active player is not under check");
        } else {
            List<Piece> activePlayerPieces = getActivePlayerPieces();
            for (Piece piece: activePlayerPieces
            ) {
                Cell startingCell = piece.getPosition();
                int startingX = startingCell.getX();
                int startingY = startingCell.getY();
                Set<Cell> availableMoves = piece.getMovesAndTargets();
                for (Cell targetCell: availableMoves
                     ) {
                    Game simulation = createSimulation();
                    Cell simulationStartingCell = simulation.getBoard().getCellByCoordinates(startingX, startingY);
                    Cell simulationTargetCell = simulation.getBoard().getCellByCoordinates(targetCell.getX(), targetCell.getY());
                    try {
                        simulation.playTurnWithoutCheckingForGameOver(simulationStartingCell, simulationTargetCell);
                        System.out.println(String.format("Can escape check with %s", piece));
                        return true;
                    } catch (IllegalArgumentException e) {
                        System.out.println("Illegal move processed");
                    }
                }
            }
            return false;
        }
    }

    public void startNewGame() {
        this.activePlayer = whitePlayer;
        board.initializePieces();
        this.turnCount = 1;
        this.movesHistory = new ArrayList<>();
        this.over = false;
        this.statusMessage = "models.Game is ongoing";
    }
}