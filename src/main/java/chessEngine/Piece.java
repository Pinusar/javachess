package chessEngine;

import java.util.HashSet;
import java.util.Set;

public abstract class Piece {
    Color color;
    Cell position;
    int value;

    public Color getColor() {
        return color;
    }

    public Color getOppositeColor() {
        if (this.color == Color.WHITE) {
            return Color.BLACK;
        } else {
            return Color.WHITE;
        }
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Cell getPosition() {
        return position;
    }

    public void setPosition(Cell position) {
        this.position = position;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public abstract Set<Cell> getMoves();

    public void move(Cell cell) {
        Set<Cell> allowedMoves = getMoves();
        if (allowedMoves.contains(cell)) {
            this.position.clear();
            this.position = cell;
            cell.setPiece(this);
        } else {
            throw new IllegalArgumentException();
        }
    };

    public void place(Cell cell) {
        if (cell.isWithinLimits()) {
            this.position.clear();
            this.position = cell;
            cell.setPiece(this);
        } else {
            throw new IllegalArgumentException("Can't place a piece on cell that's not within limits");
        }
    }

    public abstract Set<Cell> getTargets();

    public Set<Cell> getMovesAndTargets() {
        Set<Cell> movesAndTargets = new HashSet<>();
        movesAndTargets.addAll(this.getMoves());
        movesAndTargets.addAll(this.getTargets());
        return movesAndTargets;
    }

    public void capture(Cell targetCell) {
        Set<Cell> validTargets = getTargets();
        if (validTargets.contains(targetCell)) {
            Board board = this.position.getBoard();
            this.position.clear();
            board.removePiece(targetCell);
            this.position = targetCell;
            targetCell.setPiece(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void moveOrCapture(Cell targetCell) {
        if (this.getMoves().contains(targetCell)) {
            move(targetCell);
        } else {
            capture(targetCell);
        }
    }
}
