package chessEngine;

import java.util.HashSet;
import java.util.Set;

public class Bishop extends FarMovingPiece {

    public Bishop(Color color) {
        this.color = color;
        this.value = 3;
        this.position = null;
    }

    @Override
    public Set<Cell> getMoves() {
        Set<Cell> moves = new HashSet<>();
        Set<Cell> southWestMoves = getMovesByDirection(1, -1);
        Set<Cell> southEastMoves = getMovesByDirection(1, 1);
        Set<Cell> northWestMoves = getMovesByDirection(-1, -1);
        Set<Cell> northEastMoves = getMovesByDirection(-1, 1);
        moves.addAll(southWestMoves);
        moves.addAll(southEastMoves);
        moves.addAll(northWestMoves);
        moves.addAll(northEastMoves);
        return moves;
    }

    @Override
    public Set<Cell> getTargets() {
        Set<Cell> targets = new HashSet<>();
        Set<Cell> southWestTargets = getTargetsByDirection(1, -1);
        Set<Cell> southEastTargets = getTargetsByDirection(1, 1);
        Set<Cell> northWestTargets = getTargetsByDirection(-1, -1);
        Set<Cell> northEastTargets = getTargetsByDirection(-1, 1);
        targets.addAll(southWestTargets);
        targets.addAll(southEastTargets);
        targets.addAll(northWestTargets);
        targets.addAll(northEastTargets);
        return targets;
    }

    @Override
    public String toString() {
        if (this.color.equals(Color.WHITE)) {
            return "B";
        } else {
            return "b";
        }
    }
}
