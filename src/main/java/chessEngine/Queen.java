package chessEngine;

import java.util.HashSet;
import java.util.Set;

public class Queen extends FarMovingPiece {
    public Queen(Color color) {
        this.color = color;
        this.value = 9;
        this.position = null;
    }

    @Override
    public Set<Cell> getMoves() {
        Set<Cell> moves = new HashSet<>();
        Set<Cell> southMoves = getMovesByDirection(1, 0);
        Set<Cell> eastMoves = getMovesByDirection(0, 1);
        Set<Cell> westMoves = getMovesByDirection(0, -1);
        Set<Cell> northMoves = getMovesByDirection(-1, 0);
        Set<Cell> southWestMoves = getMovesByDirection(1, -1);
        Set<Cell> southEastMoves = getMovesByDirection(1, 1);
        Set<Cell> northWestMoves = getMovesByDirection(-1, -1);
        Set<Cell> northEastMoves = getMovesByDirection(-1, 1);
        moves.addAll(southMoves);
        moves.addAll(eastMoves);
        moves.addAll(westMoves);
        moves.addAll(northMoves);
        moves.addAll(southWestMoves);
        moves.addAll(southEastMoves);
        moves.addAll(northWestMoves);
        moves.addAll(northEastMoves);
        return moves;
    }

    @Override
    public Set<Cell> getTargets() {
        Set<Cell> targets = new HashSet<>();
        Set<Cell> southTargets = getTargetsByDirection(1, 0);
        Set<Cell> eastTargets = getTargetsByDirection(0, 1);
        Set<Cell> westTargets = getTargetsByDirection(0, -1);
        Set<Cell> northTargets = getTargetsByDirection(-1, 0);
        Set<Cell> southWestTargets = getTargetsByDirection(1, -1);
        Set<Cell> southEastTargets = getTargetsByDirection(1, 1);
        Set<Cell> northWestTargets = getTargetsByDirection(-1, -1);
        Set<Cell> northEastTargets = getTargetsByDirection(-1, 1);
        targets.addAll(southTargets);
        targets.addAll(eastTargets);
        targets.addAll(westTargets);
        targets.addAll(northTargets);
        targets.addAll(southWestTargets);
        targets.addAll(southEastTargets);
        targets.addAll(northWestTargets);
        targets.addAll(northEastTargets);
        return targets;
    }

    @Override
    public String toString() {
        if (this.color.equals(Color.WHITE)) {
            return "Q";
        } else {
            return "q";
        }
    }
}
