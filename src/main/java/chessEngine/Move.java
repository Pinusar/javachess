package chessEngine;

import java.util.HashMap;

public class Move {

    private String moveString;
    private Cell startingCell;
    private Board board;
    private Cell targetCell;

    private static HashMap<String, Integer> letterDictionary = new HashMap<>();
    private static HashMap<Integer, String> numberDictionary = new HashMap<>();

    static {
        letterDictionary.put("a", 0);
        letterDictionary.put("b", 1);
        letterDictionary.put("c", 2);
        letterDictionary.put("d", 3);
        letterDictionary.put("e", 4);
        letterDictionary.put("f", 5);
        letterDictionary.put("g", 6);
        letterDictionary.put("h", 7);

        numberDictionary.put(0, "a");
        numberDictionary.put(1, "b");
        numberDictionary.put(2, "c");
        numberDictionary.put(3, "d");
        numberDictionary.put(4, "e");
        numberDictionary.put(5, "f");
        numberDictionary.put(6, "g");
        numberDictionary.put(7, "h");
    }

    public Move(String moveString, Board board) {
        this.moveString = moveString;
        this.board = board;
        this.startingCell = getStartingCellFromMoveString(moveString);
        this.targetCell = getTargetCellFromMoveString(moveString);
    }

    public Move(Cell startingCell, Cell targetCell, Board board) {
        this.moveString = getMoveStringFromCells(startingCell, targetCell);
        this.board = board;
        this.startingCell = startingCell;
        this.targetCell = targetCell;
    }

    public String getMoveString() {
        return moveString;
    }

    public Cell getStartingCell() {
        return startingCell;
    }

    public Board getBoard() {
        return board;
    }

    public Cell getTargetCell() {
        return targetCell;
    }

    public Cell getStartingCellFromMoveString(String move) {
        char[] moveChars = move.toCharArray();
        int startingX = Integer.parseInt(String.valueOf(moveChars[1])) - 1;
        int startingY = letterDictionary.get(String.valueOf(moveChars[0]));
        Cell startingCell = board.getCellByCoordinates(startingX, startingY);
        return startingCell;
    }

    public Cell getTargetCellFromMoveString(String move) {
        char[] moveChars = move.toCharArray();
        int targetX = Integer.parseInt(String.valueOf(moveChars[4])) - 1;
        int targetY = letterDictionary.get(String.valueOf(moveChars[3]));
        Cell targetCell = board.getCellByCoordinates(targetX, targetY);
        return targetCell;
    }

    public String getMoveStringFromCells(Cell startingCell, Cell targetCell) {
        int startingX = startingCell.getX();
        int startingY = startingCell.getY();
        int targetX = targetCell.getX();
        int targetY = targetCell.getY();

        String moveString = String.format("%s%d %s%d", numberDictionary.get(startingY), startingX + 1, numberDictionary.get(targetY), targetX + 1);
        return  moveString;
    }

    @Override
    public String toString() {
        return moveString;
    }
}
