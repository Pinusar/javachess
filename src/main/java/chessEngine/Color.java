package chessEngine;

public enum Color {
    WHITE,
    BLACK;

    public Color getOpposite() {
        if (this == WHITE) {
            return BLACK;
        } else {
            return WHITE;
        }
    }
}
