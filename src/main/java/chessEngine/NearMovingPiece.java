package chessEngine;

import java.util.HashSet;
import java.util.Set;

public abstract class NearMovingPiece extends Piece {

    @Override
    public Set<Cell> getMoves() {
        Set<Cell> possibleMoves = getPossibleMoves();
        Set<Cell> moves = new HashSet<>();

        for (Cell targetCell : possibleMoves
        ) {
            if (targetCell.isWithinLimits() && targetCell.isEmpty()) {
                moves.add(targetCell);
            }
        }
        return moves;
    }


    Cell getMoveByDirection(int xOffSet, int yOffSet) {

        Board board = this.position.getBoard();

        int newX = this.position.getX() + xOffSet;
        int newY = this.position.getY() + yOffSet;

        if (Cell.areCoordinatesValid(newX, newY)) {
            Cell targetCell1 = board.getCellByCoordinates(newX, newY);
            return targetCell1;
        }
        return null;
    }

    abstract Set<Cell> getPossibleMoves();

    @Override
    public Set<Cell> getTargets() {
        Set<Cell> possibleTargets = getPossibleMoves();
        Set<Cell> targets = new HashSet<>();

        for (Cell targetCell : possibleTargets
        ) {
            if (targetCell.isWithinLimits() && !targetCell.isEmpty() && targetCell.containsPieceOfOppositeColor(this.color)) {
                targets.add(targetCell);
            }
        }
        return targets;
    }
}
