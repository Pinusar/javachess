package chessEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Cell {
    public int x;
    public int y;
    private Piece piece;
    private Board board;
    public static Map<Integer, String> lettermap;

    static {
        lettermap = new HashMap<>();
        lettermap.put(0, "A");
        lettermap.put(1, "B");
        lettermap.put(2, "C");
        lettermap.put(3, "D");
        lettermap.put(4, "E");
        lettermap.put(5, "F");
        lettermap.put(6, "G");
        lettermap.put(7, "H");
    }

    public Cell(int x, int y, Board board) {
        this.x = x;
        this.y = y;
        this.board = board;
    }

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
        piece.position = this;
    }

    public Board getBoard() {
        return board;
    }

    @Override
    public String toString() {
        String letter = lettermap.get(this.y);
        String number = String.valueOf(this.x + 1);
        String result = String.format("%s%s", letter, number);
        return result;
    }

    public boolean isWithinLimits() {
        if (x < 0 || y < 0) {
            return false;
        }
        if (x > 7 || y > 7) {
            return false;
        }
        return true;
    }

    public static boolean areCoordinatesValid(int x, int y) {
        if (x < 0 || y < 0) {
            return false;
        }
        if (x > 7 || y > 7) {
            return false;
        }
        return true;
    }

    public boolean isEmpty() {
        return piece == null;
    }

    public boolean containsPieceByColor(Color color) {
        return !isEmpty() && piece.color == color;
    }

    public boolean containsPieceOfOppositeColor(Color color) {
        return !isEmpty() && piece.color != color;
    }

    public void clear() {
        this.piece = null;
    }

    public boolean isUnderCheckFromColor(Color color) {
        List<Piece> opponentPieces = board.getPiecesByColor(color);
        if (this.isEmpty()) {
            Pawn dummyPawn = new Pawn(color.getOpposite());
            this.board.putPiece(dummyPawn, this);
            boolean result = isUnderCheckByPieces(opponentPieces);
            this.board.removePiece(this);
            return result;
        } else {
            return isUnderCheckByPieces(opponentPieces);
        }
    }

    private boolean isUnderCheckByPieces(List<Piece> opponentPieces) {
        for (Piece piece : opponentPieces
        ) {
            if (piece.getTargets().contains(this)) {
                System.out.println(String.format("models.Cell is under check by %s at %s", piece, piece.position));
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Cell) {
            Cell cell = (Cell) obj;
            return this.x == cell.x && this.y == cell.y;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.x, this.y);
    }
}
