package chessEngine;

import java.util.HashSet;
import java.util.Set;

public abstract class FarMovingPiece extends Piece {

    Set<Cell> getMovesByDirection(int xOffSet, int yOffSet) {
        Set<Cell> moves = new HashSet<>();
        Board board = this.position.getBoard();
        Cell currentCell = this.position;
        int nextX = currentCell.x + xOffSet;
        int nextY = currentCell.y + yOffSet;
        while (Cell.areCoordinatesValid(nextX, nextY) && board.getCellByCoordinates(nextX, nextY).isEmpty()) {
            Cell nextCell = board.getCellByCoordinates(nextX, nextY);
            moves.add(nextCell);
            nextX += xOffSet;
            nextY += yOffSet;
        }
        return moves;
    }

    Set<Cell> getTargetsByDirection(int xOffSet, int yOffSet) {
        Set<Cell> targets = new HashSet<>();
        Board board = this.position.getBoard();
        Cell currentCell = this.position;
        int nextX = currentCell.x + xOffSet;
        int nextY = currentCell.y + yOffSet;

        while (Cell.areCoordinatesValid(nextX, nextY)
                && (board.getCellByCoordinates(nextX, nextY).isEmpty() || board.getCellByCoordinates(nextX, nextY).containsPieceOfOppositeColor(this.color))) {
            if (board.getCellByCoordinates(nextX, nextY).containsPieceOfOppositeColor(this.color)) {
                Cell nextCell = board.getCellByCoordinates(nextX, nextY);
                targets.add(nextCell);
                return targets;
            } else {
                nextX += xOffSet;
                nextY += yOffSet;
            }
        }
        ;
        return targets;
    }
}
