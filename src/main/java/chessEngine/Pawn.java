package chessEngine;

import java.util.HashSet;
import java.util.Set;

public class Pawn extends Piece {

    private int movedDoubleTimer;

    public Pawn(Color color) {
        this.color = color;
        this.position = null;
        this.value = 1;
        this.movedDoubleTimer = 0;
    }

    public int getRank() {
        if (color == Color.WHITE) {
            return position.getX() + 1;
        } else {
            return 8 - position.getX();
        }
    }

    public int getMovedDoubleTimer() {
        return movedDoubleTimer;
    }

    public void setMovedDoubleTimer(int movedDoubleTimer) {
        this.movedDoubleTimer = movedDoubleTimer;
    }

    @Override
    public Set<Cell> getMoves() {
        Board board = position.getBoard();
        Set<Cell> moves = new HashSet<>();
        if (getRank() > 7) {
            return moves;
        }
        if (color == Color.WHITE) {
            Cell oneCellMove = board.getCellByCoordinates(position.x + 1, position.y);
            if (oneCellMove.isEmpty()) {
                moves.add(oneCellMove);
                if (getRank() == 2) {
                    Cell twoCellMove = board.getCellByCoordinates(position.x + 2, position.y);
                    if (twoCellMove.isEmpty()) {
                        moves.add(twoCellMove);
                    }
                }
            }
        } else {
            Cell oneCellMove = board.getCellByCoordinates(position.x - 1, position.y);
            if (oneCellMove.isEmpty()) {
                moves.add(oneCellMove);
                if (getRank() == 2) {
                    Cell twoCellMove = board.getCellByCoordinates(position.x - 2, position.y);
                    if (twoCellMove.isEmpty()) {
                        moves.add(twoCellMove);
                    }
                }
            }
        }
        return moves;
    }

    @Override
    public void move(Cell cell) {
        Set<Cell> allowedMoves = getMoves();
        if (allowedMoves.contains(cell)) {
            if (Math.abs(cell.getX() - this.position.getX()) > 1) {
                this.movedDoubleTimer = 2;
            }
            this.position.clear();
            this.position = cell;
            cell.setPiece(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    ;

    @Override
    public Set<Cell> getTargets() {
        Set<Cell> targets = new HashSet<>();
        Board board = position.getBoard();
        if (getRank() > 7) {
            return targets;
        }
        if (color == Color.WHITE) {
            if (Cell.areCoordinatesValid(position.x + 1, position.y - 1)) {
                Cell target1 = board.getCellByCoordinates(position.x + 1, position.y - 1);
                if (target1.containsPieceOfOppositeColor(Color.WHITE)) {
                    targets.add(target1);
                }
            }
            if (Cell.areCoordinatesValid(position.x + 1, position.y + 1)) {
                Cell target2 = board.getCellByCoordinates(position.x + 1, position.y + 1);
                if (target2.containsPieceOfOppositeColor(Color.WHITE)) {
                    targets.add(target2);
                }
            }
        } else {
            if (Cell.areCoordinatesValid(position.x - 1, position.y - 1)) {
                Cell target1 = board.getCellByCoordinates(position.x - 1, position.y - 1);
                if (target1.containsPieceOfOppositeColor(Color.BLACK)) {
                    targets.add(target1);
                }
            }
            if (Cell.areCoordinatesValid(position.x - 1, position.y + 1)) {
                Cell target2 = board.getCellByCoordinates(position.x - 1, position.y + 1);
                if (target2.containsPieceOfOppositeColor(Color.BLACK)) {
                    targets.add(target2);
                }
            }
        }
        return targets;
    }

    public Set<Cell> getEnPassantTargets() {
        Set<Cell> targets = new HashSet<>();
        if (getRank() == 5) {
            Board board = this.position.getBoard();
            Cell target1 = board.getCellByCoordinates(position.getX(), position.getY() - 1);
            if (!target1.isEmpty()) {
                Piece piece = target1.getPiece();
                if (piece instanceof Pawn && piece.color != this.color && ((Pawn) piece).getMovedDoubleTimer() > 0) {
                    Cell destinationCell1 = getEnPassantDestination(target1);
                    targets.add(destinationCell1);
                }
            }
            Cell target2 = board.getCellByCoordinates(position.getX(), position.getY() + 1);
            if (!target2.isEmpty()) {
                Piece piece = target2.getPiece();
                if (piece instanceof Pawn && piece.color != this.color && ((Pawn) piece).getMovedDoubleTimer() > 0) {
                    Cell destinationCell2 = getEnPassantDestination(target2);
                    targets.add(destinationCell2);
                }
            }
        }
        return targets;
    }

    @Override
    public void capture(Cell targetCell) {
        Set<Cell> regularTargets = getTargets();
        Set<Cell> enPassantTargets = getEnPassantTargets();
        if (regularTargets.contains(targetCell)) {
            Board board = this.position.getBoard();
            this.position.clear();
            board.removePiece(targetCell);
            this.position = targetCell;
            targetCell.setPiece(this);
        } else if (enPassantTargets.contains(targetCell)) {
            Board board = this.position.getBoard();
            Cell opposingPieceCell;
            if (this.color == Color.WHITE) {
                opposingPieceCell = board.getCellByCoordinates(targetCell.getX() - 1, targetCell.getY());
            } else {
                opposingPieceCell = board.getCellByCoordinates(targetCell.getX() + 1, targetCell.getY());
            }
            this.position.clear();
            board.removePiece(opposingPieceCell);
            this.position = targetCell;
            targetCell.setPiece(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private Cell getEnPassantDestination(Cell targetCell) {
        Board board = this.position.getBoard();
        Cell destinationCell;
        if (this.color == Color.WHITE) {
            destinationCell = board.getCellByCoordinates(targetCell.getX() + 1, targetCell.getY());
        } else {
            destinationCell = board.getCellByCoordinates(targetCell.getX() - 1, targetCell.getY());
        }
        return destinationCell;
    }

    public void reduceDoubleMoveTimer() {
        this.movedDoubleTimer--;
    }

    @Override
    public String toString() {
        if (this.color.equals(Color.WHITE)) {
            return "P";
        } else {
            return "p";
        }
    }
}
