package chessEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Board {
    private List<Piece> pieces;
    Cell[][] cells;

    public Board() {
        this.pieces = new ArrayList<>();
        this.cells = new Cell[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Cell cell = new Cell(i, j, this);
                cells[i][j] = cell;
            }
        }
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public List<Piece> getPiecesByColor(Color color) {
        return getPieces().stream()
                .filter(piece -> piece.color == color)
                .collect(Collectors.toList());
    }

    public int getMaterialValueByColor(Color color) {
        List <Piece> pieces = getPiecesByColor(color);
        int total = pieces.stream().reduce(0, (subtotal, piece) -> subtotal + piece.getValue(), Integer::sum);
        return total;
    }

    public List<Piece> getPawns() {
        return getPieces().stream()
                .filter(piece -> piece instanceof Pawn)
                .collect(Collectors.toList());
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell getCellByCoordinates(int x, int y) {
        return cells[x][y];
    }

    public void putPiece(Piece piece, Cell cell) {
        this.cells[cell.x][cell.y].setPiece(piece);
        this.pieces.add(piece);
    }

    public Piece getPieceByCell(Cell cell) {
        return this.cells[cell.x][cell.y].getPiece();
    }

    public void removePiece(Cell cell) {
        Piece piece = this.getPieceByCell(cell);
        this.cells[cell.x][cell.y].clear();
        this.pieces.remove(piece);
    }

    public void removeAllPieces() {
        for (Cell[] row: cells
             ) {
            for (Cell cell: row
                 ) {
                if (!cell.isEmpty()) {
                    Piece piece = cell.getPiece();
                    pieces.remove(piece);
                }
                cell.clear();
            }
        }
    }

    public void initializePieces() {
        removeAllPieces();

        Rook whiteRook1 = new Rook(Color.WHITE);
        putPiece(whiteRook1, getCellByCoordinates(0, 0));
        Rook whiteRook2 = new Rook(Color.WHITE);
        putPiece(whiteRook2, getCellByCoordinates(0, 7));

        Knight whiteKnight1 = new Knight(Color.WHITE);
        putPiece(whiteKnight1, getCellByCoordinates(0, 1));
        Knight whiteKnight2 = new Knight(Color.WHITE);
        putPiece(whiteKnight2, getCellByCoordinates(0, 6));

        Bishop whiteBishop1 = new Bishop(Color.WHITE);
        putPiece(whiteBishop1, getCellByCoordinates(0, 2));
        Bishop whiteBishop2 = new Bishop(Color.WHITE);
        putPiece(whiteBishop2, getCellByCoordinates(0, 5));

        Queen whiteQueen = new Queen(Color.WHITE);
        putPiece(whiteQueen, getCellByCoordinates(0, 3));

        King whiteKing = new King(Color.WHITE);
        putPiece(whiteKing, getCellByCoordinates(0, 4));

        for (int i = 0; i < 8; i++) {
            Pawn whitePawn = new Pawn(Color.WHITE);
            putPiece(whitePawn, getCellByCoordinates(1, i));
        }

        Rook blackRook1 = new Rook(Color.BLACK);
        putPiece(blackRook1, getCellByCoordinates(7, 0));
        Rook blackRook2 = new Rook(Color.BLACK);
        putPiece(blackRook2, getCellByCoordinates(7, 7));

        Knight blackKnight1 = new Knight(Color.BLACK);
        putPiece(blackKnight1, getCellByCoordinates(7, 1));
        Knight blackKnight2 = new Knight(Color.BLACK);
        putPiece(blackKnight2, getCellByCoordinates(7, 6));

        Bishop blackBishop1 = new Bishop(Color.BLACK);
        putPiece(blackBishop1, getCellByCoordinates(7, 2));
        Bishop blackBishop2 = new Bishop(Color.BLACK);
        putPiece(blackBishop2, getCellByCoordinates(7, 5));

        Queen blackQueen = new Queen(Color.BLACK);
        putPiece(blackQueen, getCellByCoordinates(7, 3));

        King blackKing = new King(Color.BLACK);
        putPiece(blackKing, getCellByCoordinates(7, 4));

        for (int i = 0; i < 8; i++) {
            Pawn blackPawn = new Pawn(Color.BLACK);
            putPiece(blackPawn, getCellByCoordinates(6, i));
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("  A  B  C  D  E  F  G  H\n");
        for (int i = 0; i < 8; i++) {
            result.append(String.valueOf(i + 1));
            Cell[] row = this.cells[i];
            for (Cell cell : row
            ) {
                Piece piece = cell.getPiece();
                if (piece != null) {
                    result.append(String.format(" %s ", piece));
                } else {
                    result.append(" . ");
                }
            }
            result.append("\n");
        }
        return result.toString();
    }

    public Rook findRookByCoordinates(int x, int y) {
        Cell rookCell = getCellByCoordinates(x, y);
        Piece targetPiece = getPieceByCell(rookCell);
        if (targetPiece instanceof Rook) {
            Rook targetRook = (Rook) targetPiece;
            return targetRook;
        } else {
            throw new IllegalArgumentException("No rook found!");
        }
    }

    public String getHtmlString() {
        String s = this.toString();
        String html = s.replace("\n", "<br>");
        return html;
    }

}
