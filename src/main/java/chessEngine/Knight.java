package chessEngine;

import java.util.HashSet;
import java.util.Set;

public class Knight extends NearMovingPiece {

    public Knight(Color color) {
        this.color = color;
        this.position = null;
        this.value = 3;
    }

    @Override
    Set<Cell> getPossibleMoves() {
        Set<Cell> allMoves = new HashSet<>();
        Set<Cell> possibleMoves = new HashSet<>();

        allMoves.add(getMoveByDirection(-1, -2));
        allMoves.add(getMoveByDirection(-1, 2));
        allMoves.add(getMoveByDirection(1, -2));
        allMoves.add(getMoveByDirection(1, 2));
        allMoves.add(getMoveByDirection(-2, -1));
        allMoves.add(getMoveByDirection(-2, 1));
        allMoves.add(getMoveByDirection(2, -1));
        allMoves.add(getMoveByDirection(2, 1));

        for (Cell move: allMoves
             ) {
            if (move != null) {
                possibleMoves.add(move);
            }
        }

        return possibleMoves;
    }

    @Override
    public String toString() {
        if (this.color.equals(Color.WHITE)) {
            return "N";
        } else {
            return "n";
        }
    }
}
