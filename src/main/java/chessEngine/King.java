package chessEngine;

import java.util.HashSet;
import java.util.Set;

public class King extends NearMovingPiece {

    private boolean moved;

    public King(Color color) {
        this.color = color;
        this.position = null;
        this.value = 0;
        this.moved = false;
    }

    public boolean hasMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    @Override
    Set<Cell> getPossibleMoves() {
        Set<Cell> allMoves = new HashSet<>();
        Set<Cell> possibleMoves = new HashSet<>();

        allMoves.add(getMoveByDirection(-1, -1));
        allMoves.add(getMoveByDirection(-1, 0));
        allMoves.add(getMoveByDirection(-1, 1));
        allMoves.add(getMoveByDirection(1, -1));
        allMoves.add(getMoveByDirection(1, 0));
        allMoves.add(getMoveByDirection(1, 1));
        allMoves.add(getMoveByDirection(0, -1));
        allMoves.add(getMoveByDirection(0, 1));

        for (Cell move : allMoves
        ) {
            if (move != null) {
                possibleMoves.add(move);
            }
        }

        return possibleMoves;
    }

    @Override
    public void move(Cell cell) {
        Set<Cell> allowedMoves = getMoves();
        if (allowedMoves.contains(cell)) {
            this.position.clear();
            this.position = cell;
            cell.setPiece(this);
            moved = true;
        } else {
            throw new IllegalArgumentException();
        }
    }


    public void castle(Rook targetRook) {
        if (canCastle(targetRook)) {
            int rookY = targetRook.position.getY();
            if (rookY == 7) {
                castleKingside(targetRook);
            } else if (rookY == 0) {
                castleQueenside(targetRook);
            } else {
                throw new IllegalArgumentException("models.Rook position is illegal for castling!");
            }
        } else {
            throw new IllegalArgumentException("Illegal castling attempt!");
        }
    }


    private void castleKingside(Rook targetRook) {
        Board board = this.position.getBoard();
        Cell kingTargetCell = board.getCellByCoordinates(this.position.getX(), 6);
        Cell rookTargetCell = board.getCellByCoordinates(this.position.getX(), 5);
        this.place(kingTargetCell);
        targetRook.place(rookTargetCell);
    }

    private void castleQueenside(Rook targetRook) {
        Board board = this.position.getBoard();
        Cell kingTargetCell = board.getCellByCoordinates(this.position.getX(), 2);
        Cell rookTargetCell = board.getCellByCoordinates(this.position.getX(), 3);
        this.place(kingTargetCell);
        targetRook.place(rookTargetCell);
        ;
    }

    public boolean canCastle(Rook targetRook) {
        return targetRook.color == this.color && !this.hasMoved() && !targetRook.hasMoved() && isCastlingPathFree(targetRook) && isCastlingPathSafe(targetRook);
    }

    public boolean isCastlingPathFree(Rook targetRook) {
        Board board = this.position.getBoard();
        Cell rookCell = targetRook.getPosition();
        int targetY = rookCell.getY();
        int startingY = this.position.getY();
        if (startingY < targetY) {
            for (int y = startingY + 1; y < targetY; y++) {
                Cell cell = board.getCellByCoordinates(this.position.getX(), y);
                if (!cell.isEmpty()) {
                    return false;
                }
            }
        } else {
            for (int y = startingY - 1; y > targetY; y--) {
                Cell cell = board.getCellByCoordinates(this.position.getX(), y);
                if (!cell.isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isCastlingPathSafe(Rook targetRook) {
        Board board = this.position.getBoard();
        int startingY = this.position.getY();
        int targetY = getKingCastlingTargetY(targetRook);
        if (startingY < targetY) {
            for (int y = startingY + 1; y < targetY; y++) {
                Cell cell = board.getCellByCoordinates(this.position.getX(), y);
                if (cell.isUnderCheckFromColor(this.getOppositeColor())) {
                    return false;
                }
            }
        } else {
            for (int y = startingY - 1; y > targetY; y--) {
                Cell cell = board.getCellByCoordinates(this.position.getX(), y);
                if (cell.isUnderCheckFromColor(this.getOppositeColor())) {
                    return false;
                }
            }
        }
        return true;
    }

    private int getKingCastlingTargetY(Rook targetRook) {
        int targetY;
        if (targetRook.getPosition().getY() > this.getPosition().getX()) {
            targetY = this.getPosition().getY() + 2;
        } else {
            targetY = this.getPosition().getY() - 2;
        }
        return targetY;
    }


    @Override
    public String toString() {
        if (this.color.equals(Color.WHITE)) {
            return "K";
        } else {
            return "k";
        }
    }
}
