package chessEngine;

import java.util.HashSet;
import java.util.Set;

public class Rook extends FarMovingPiece {

    private boolean moved;

    public Rook(Color color) {
        this.color = color;
        this.value = 5;
        this.position = null;
        this.moved = false;
    }

    public boolean hasMoved() {
        return moved;
    }

    public void setMoved(boolean moved) {
        this.moved = moved;
    }

    @Override
    public Set<Cell> getMoves() {
        Set<Cell> moves = new HashSet<>();
        Set<Cell> southMoves = getMovesByDirection(1, 0);
        Set<Cell> eastMoves = getMovesByDirection(0, 1);
        Set<Cell> westMoves = getMovesByDirection(0, -1);
        Set<Cell> northMoves = getMovesByDirection(-1, 0);
        moves.addAll(southMoves);
        moves.addAll(eastMoves);
        moves.addAll(westMoves);
        moves.addAll(northMoves);
        return moves;
    }

    @Override
    public void move(Cell cell) {
        Set<Cell> allowedMoves = getMoves();
        if (allowedMoves.contains(cell)) {
            this.position.clear();
            this.position = cell;
            cell.setPiece(this);
            moved = true;
        } else {
            throw new IllegalArgumentException();
        }
    };

    @Override
    public Set<Cell> getTargets() {
        Set<Cell> targets = new HashSet<>();
        Set<Cell> southTargets = getTargetsByDirection(1, 0);
        Set<Cell> eastTargets = getTargetsByDirection(0, 1);
        Set<Cell> westTargets = getTargetsByDirection(0, -1);
        Set<Cell> northTargets = getTargetsByDirection(-1, 0);
        targets.addAll(southTargets);
        targets.addAll(eastTargets);
        targets.addAll(westTargets);
        targets.addAll(northTargets);
        return targets;
    }

    @Override
    public String toString() {
        if (this.color.equals(Color.WHITE)) {
            return "R";
        } else {
            return "r";
        }
    }
}
