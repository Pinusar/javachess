package chessEngine;

import java.util.List;

public class Player {
    private Color color;
    private Board board;

    public Player(Color color, Board board) {
        this.color = color;
        this.board = board;
    }

    public Color getColor() {
        return color;
    }

    public Board getBoard() {
        return board;
    }

    public boolean isUnderCheck() {
        King king = getKing();
        Cell kingPosition = king.getPosition();
        return isCellUnderCheck(kingPosition);
    }

    public boolean isCellUnderCheck(Cell cell) {
        return cell.isUnderCheckFromColor(getOppositeColor());
    }

    public List<Piece> getPieces() {
        return board.getPiecesByColor(this.color);
    }

    public King getKing() {
        King king = (King) getPieces().stream()
                .filter(piece -> piece instanceof King)
                .findFirst()
                .get();
        return king;
    }

    private Color getOppositeColor() {
        if (this.color == Color.WHITE) {
            return Color.BLACK;
        } else {
            return Color.WHITE;
        }
    }
}
